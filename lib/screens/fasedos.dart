import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/services.dart';
import 'package:vcount/ui/styles.dart';

class FaseDos extends StatefulWidget {
  FaseDos({Key key}) : super(key: key);

/*Este widget es la página de inicio de su aplicación.
  Tiene estado, lo que significa que tiene un objeto State (definido a continuación)
  que contiene campos que afectan su aspecto.

  Esta clase es la configuración del estado.
  Contiene los valores (en este caso, el título) proporcionados por el padre
  (en este caso, el widget de la aplicación) y utilizados por el método de compilación del Estado.
  Los campos en una subclase de Widget siempre se marcan como "final".*/

  @override
  _FaseDosState createState() => _FaseDosState();
}

class _FaseDosState extends State<FaseDos> {
  int counterCamion = 0;
  int counterMoto = 0;
  int counterTraffic = 0;
  int counterColectivo = 0;
  int counterBicicleta = 0;

//----------------------STOPWATCH----------------------
  final _stopWatch = Stopwatch();
  final _timeout = Duration(seconds: 1);
  String stopWatchText = "00:00:00";
  IconData iconstatus = FontAwesomeIcons.play;

  void _startTimeout() {
    Timer(_timeout, _handleTimeout);
  }

  void _startStopButtonPressed() {
    setState(() {
      if (_stopWatch.isRunning) {
        _stopWatch.stop();
        iconstatus = FontAwesomeIcons.play;
      } else {
        _stopWatch.start();
        _startTimeout();
        iconstatus = FontAwesomeIcons.pause;
      }
    });
  }

  void _resetButtonPressed() {
    if (_stopWatch.isRunning) {
      _startStopButtonPressed();
    }
    setState(() {
      _stopWatch.reset();
      _setStopwatchText();
    });
  }

  void _handleTimeout() {
    if (_stopWatch.isRunning) {
      _startTimeout();
    }
    setState(() {
      _setStopwatchText();
    });
  }

  void _setStopwatchText() {
    stopWatchText =
        _stopWatch.elapsed.inHours.toString().padLeft(2, "0") + ":" + (_stopWatch.elapsed.inMinutes % 60).toString().padLeft(2, "0") + ":" + (_stopWatch.elapsed.inSeconds % 60).toString().padLeft(2, "0");
  }

//----------------------STOPWATCH----------------------

//----------------------SHARED PREFERENCES----------------------

  @override
  void initState() {
    super.initState();
    _cargarContadores();
  }

  //Cargando el valor del contador en el inicio
  _cargarContadores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterCamion = (prefs.getInt('counterCamion') ?? 0);
      counterMoto = (prefs.getInt('counterMoto') ?? 0);
      counterTraffic = (prefs.getInt('counterTraffic') ?? 0);
      counterColectivo = (prefs.getInt('counterColectivo') ?? 0);
      counterBicicleta = (prefs.getInt('counterBicicleta') ?? 0);
    });
  }

  _removerContadores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.remove('counterCamion');
      prefs.remove('counterMoto');
      prefs.remove('counterTraffic');
      prefs.remove('counterColectivo');
      prefs.remove('counterBicicleta');
      counterCamion = 0;
      counterMoto = 0;
      counterTraffic = 0;
      counterColectivo = 0;
      counterBicicleta = 0;
    });
  }

  //Incrementando el contador después del clic
  _incrementarContadorCamion() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterCamion = (prefs.getInt('counterCamion') ?? 0) + 1;
      prefs.setInt('counterCamion', counterCamion);
    });
  }

  //Incrementando el contador después del clic
  _incrementarContadorMoto() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterMoto = (prefs.getInt('counterMoto') ?? 0) + 1;
      prefs.setInt('counterMoto', counterMoto);
    });
  }

  _incrementarContadorTraffic() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterTraffic = (prefs.getInt('counterTraffic') ?? 0) + 1;
      prefs.setInt('counterTraffic', counterTraffic);
    });
  }

  //Incrementando el contador después del clic
  _incrementarContadorColectivo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterColectivo = (prefs.getInt('counterColectivo') ?? 0) + 1;
      prefs.setInt('counterColectivo', counterColectivo);
    });
  }

  //Incrementando el contador después del clic
  _incrementarContadorBicicleta() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterBicicleta = (prefs.getInt('counterBicicleta') ?? 0) + 1;
      prefs.setInt('counterBicicleta', counterBicicleta);
    });
  }

//----------------------SHARED PREFERENCES----------------------

  @override
  Widget build(BuildContext context) {
    /*Este método se vuelve a ejecutar cada vez que se llama a setState, por ejemplo,
    como lo hace el método _incrementCounter anterior.

    El marco Flutter se ha optimizado para acelerar la ejecución de los métodos de compilación,
    para que pueda reconstruir cualquier cosa que necesita actualizarse
    en lugar de tener que cambiar individualmente las instancias de widgets*/
    return WillPopScope(
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
                SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
              },
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            elevation: 0.0,
            // BOTON PLAY-PAUSA
            centerTitle: true,
            title: RaisedButton(
              child: Icon(iconstatus),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              onPressed: () {
                _startStopButtonPressed();
              },
              onLongPress: () {
                _resetButtonPressed();
              },
            ),
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                // ROW VEHICULOS
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Expanded(child: Padding(padding: EdgeInsets.all(5), child: buildVehicleButton("Camión", iconCamion(), counterCamion))),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Expanded(child: Padding(padding: EdgeInsets.all(5), child: buildVehicleButton("Moto", iconMoto(), counterMoto))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: Padding(padding: EdgeInsets.all(5), child: buildVehicleButton("Colectivo", iconColectivo(), counterColectivo))),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Expanded(child: Padding(padding: EdgeInsets.all(5), child: buildVehicleButton("Traffic", iconTraffic(), counterTraffic))),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Expanded(child: Padding(padding: EdgeInsets.all(5), child: buildVehicleButton("Bicicleta", iconBicicleta(), counterBicicleta))),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Text(stopWatchText, style: bigSubtitle()),
              ],
            ),
          ),
        ),
      ),
      onWillPop: () {
        Navigator.pop(context);
        return SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      },
    );
  }

  RaisedButton buildVehicleButton(String vehiculo, IconData icono, int valor) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Center(
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5),
                child: Icon(icono, size: MediaQuery.of(context).size.height / 5),
              ),
              Text("$vehiculo:\n$valor", style: title(), textAlign: TextAlign.center)
            ],
          ),
        ),
      ),
      onPressed: () {
        /*Esta llamada a setState le dice al marco Flutter que algo ha cambiado en este estado,
    lo que hace que vuelva a ejecutar el siguiente método de compilación para que la pantalla
    pueda reflejar los valores actualizados. Si cambiamos _counter sin llamar a setState(),
    entonces no se volvería a llamar al método de compilación, por lo que no parecería que pasara nada.*/

        switch (vehiculo) {
          case "Camión":
            {
              setState(() {
                _incrementarContadorCamion();
              });
            }
            break;

          case "Moto":
            {
              setState(() {
                _incrementarContadorMoto();
              });
            }
            break;

          case "Traffic":
            {
              setState(() {
                _incrementarContadorTraffic();
              });
            }
            break;

          case "Colectivo":
            {
              setState(() {
                _incrementarContadorColectivo();
              });
            }
            break;

          case "Bicicleta":
            {
              setState(() {
                _incrementarContadorBicicleta();
              });
            }
            break;

          default:
            {
              //statements;
            }
            break;
        }
      },
      onLongPress: () {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text("Eliminar contador", style: title()),
            content: Text("¿Está seguro que desea eliminar TODOS los contadores?\nEsta acción no se puede deshacer", style: subHeader()),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("NO", style: subHeader())),
              FlatButton(
                  onPressed: () {
                    _removerContadores();
                    Navigator.of(context).pop();
                  },
                  child: Text("SI", style: subHeader())),
            ],
          ),
          barrierDismissible: true,
        );
      },
    );
  }
}
