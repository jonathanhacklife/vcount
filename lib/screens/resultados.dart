import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class Resultados extends StatefulWidget {
  @override
  _ResultadosState createState() => _ResultadosState();
}

class _ResultadosState extends State<Resultados> {
  int counterAuto;
  int counterCamioneta;
  int counterCamion;
  int counterTraffic;
  int counterColectivo;
  int counterMoto;
  int counterBicicleta;

  _cargarContadores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counterAuto = (prefs.getInt('counterAuto') ?? 0);
      counterCamioneta = (prefs.getInt('counterCamioneta') ?? 0);
      counterCamion = (prefs.getInt('counterCamion') ?? 0);
      counterTraffic = (prefs.getInt('counterTraffic') ?? 0);
      counterColectivo = (prefs.getInt('counterColectivo') ?? 0);
      counterMoto = (prefs.getInt('counterMoto') ?? 0);
      counterBicicleta = (prefs.getInt('counterBicicleta') ?? 0);
    });
  }

  @override
  void initState() {
    super.initState();
    _cargarContadores();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Theme.of(context).scaffoldBackgroundColor, Theme.of(context).primaryColor],
              ),
            ),
            child: Center(
              child: DefaultTabController(
                length: 7,
                child: Builder(
                  builder: (BuildContext context) => Column(
                    children: <Widget>[
                      TabPageSelector(),
                      Expanded(
                        child: TabBarView(
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(image: AssetImage('assets/car.png'), height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de autos", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13),
                                    textAlign: TextAlign.center),
                                Text("$counterAuto", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5),
                                    textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                    image: AssetImage('assets/pickup.png'), height: MediaQuery.of(context).size.height /
                                    2.5),
                                Text("Cantidad de camionetas", style: TextStyle(fontSize: MediaQuery.of(context).size.width /
                                    13), textAlign: TextAlign.center),
                                Text("$counterCamioneta", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                    image: AssetImage('assets/truck.png'), height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de camiones", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13), textAlign: TextAlign.center),
                                Text("$counterCamion", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(image: AssetImage('assets/bus.png'), height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de colectivos", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13), textAlign: TextAlign.center),
                                Text("$counterColectivo", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(image: AssetImage('assets/van.png'), height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de traffics", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13), textAlign: TextAlign.center),
                                Text("$counterTraffic", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                    image: AssetImage('assets/motorcycle.png'),
                                    height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de motos", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13), textAlign: TextAlign.center),
                                Text("$counterMoto", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                    image: AssetImage('assets/bicycle.png'),
                                    height: MediaQuery.of(context).size.height / 2.5),
                                Text("Cantidad de bicicletas", style: TextStyle(fontSize: MediaQuery.of(context).size.width / 13), textAlign: TextAlign.center),
                                Text("$counterBicicleta", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 5), textAlign: TextAlign.center)
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
