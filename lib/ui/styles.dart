import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

double sizeIcono = 60;

// En este archivo guardaremos nuestros estilos personalizados para llamar en los distintos Widgets.
Icon iconConfirmado() => Icon(Icons.check_circle, color: colorValid());

Icon iconCancelado() => Icon(Icons.cancel, color: accentColor());

Icon iconNoDisponible() => Icon(Icons.not_interested, color: accentColor());

IconData iconAuto() => FontAwesomeIcons.carSide;

IconData iconCamioneta() => FontAwesomeIcons.truckPickup;

IconData iconCamion() => FontAwesomeIcons.truck;

IconData iconTraffic() => FontAwesomeIcons.shuttleVan;

IconData iconColectivo() => FontAwesomeIcons.bus;

IconData iconBicicleta() => FontAwesomeIcons.bicycle;

IconData iconMoto() => FontAwesomeIcons.motorcycle;

Icon backIcon() => Icon(Icons.arrow_back_ios);

Color colorValid() => Colors.lightGreenAccent;

Color colorBusy() => Colors.orange;

Color colorFailed() => Colors.red;

Color primaryColor() => Colors.white;

Color accentColor() => Colors.blue;

Color secondaryColor() => Colors.black;

TextStyle superTitle([Color color]) => TextStyle(fontSize: 150, color: color);

TextStyle bigTitle([Color color]) => TextStyle(fontSize: 60, color: color);

TextStyle bigSubtitle([Color color]) => TextStyle(color: color, fontSize: 33);

TextStyle headline([Color color]) => TextStyle(color: color, fontSize: 30);

TextStyle title([Color color]) => TextStyle(color: color, fontSize: 24, fontWeight: FontWeight.w600);

TextStyle subHeader([Color color]) => TextStyle(color: color, fontSize: 22);
