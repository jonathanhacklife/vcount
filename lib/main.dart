import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vcount/screens/faseuno.dart';
import 'package:vcount/screens/fasedos.dart';
import 'package:vcount/screens/resultados.dart';
import 'package:vcount/ui/styles.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]).then((_) {
    runApp(MyApp());
  });
}
bool isDark;
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'VCount',
      theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: accentColor(),
          accentColor: accentColor(),
          primarySwatch: accentColor(),
          splashColor: accentColor(),
          scaffoldBackgroundColor: Colors.black,
          fontFamily: "bahnschriftRegular"),
      home: Menuprincipal(),
    );
  }
}

class Menuprincipal extends StatefulWidget {
  Menuprincipal({Key key}) : super(key: key);

  @override
  _MenuprincipalState createState() => _MenuprincipalState();
}

class _MenuprincipalState extends State<Menuprincipal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0.0,
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.menu),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: Theme.of(context).primaryColor),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("VCount", style: bigTitle()),
                    Text("Arbusta - YPF", style: bigSubtitle()),
                  ],
                )),
            ListTile(
                title: Text("Resultados", style: subHeader()),
                leading: Icon(FontAwesomeIcons.solidStar),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Resultados()));
                }),
            ListTile(
              title: Text("Cambiar tema", style: subHeader()),
              leading: Icon(FontAwesomeIcons.adjust),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              children: <Widget>[
                RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height / 30, horizontal:
                    MediaQuery.of(context).size.width / 5),
                    child: Text("FASE\nUNO", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 13), textAlign: TextAlign.right),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FaseUno()));
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
                    }),
                Spacer()
              ],
            ),
            Row(
              children: <Widget>[
                Spacer(),
                RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height / 30, horizontal:
                  MediaQuery.of
                    (context).size.width / 5),
                  child: Text("FASE\nDOS", style: TextStyle(fontSize: MediaQuery.of(context).size.height / 13)),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(20),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => FaseDos()));
                    SystemChrome.setPreferredOrientations(
                        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
