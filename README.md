# vcount

Aplicación de conteo vehicular diseñado para Arbusta LABS e YPF. Esta aplicación es una herramienta usada para hacer un recuento vehicular en zonas específicas en las que la empresa YPF tiene sus sucursales.

<img src="docs/Screenshot_20210910-113316.png" width="300">
<img src="docs/Screenshot_20210910-112325.png" width="300">
<img src="docs/Screenshot_20210910-113340.png" width="300">
<img src="docs/Screenshot_20210910-112342.png" width="500">
<img src="docs/Screenshot_20210910-113259.png" width="500">

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
